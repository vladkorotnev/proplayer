//
//  AppDelegate.h
//  ProPlayer
//
//  Created by VirtualMachine on 09/12/2018.
//  Copyright © 2018 Genjitsu Labs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

