//
//  main.m
//  ProPlayer
//
//  Created by VirtualMachine on 09/12/2018.
//  Copyright © 2018 Genjitsu Labs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
